//
//  LangsModel.swift
//  audiEventi
//
//  Created by aleksandar.aleksic on 2.7.21..
//

import Foundation

struct Langs: Codable{
    var ADE_HEADER: String
    var C_001_NOTVALID: String
    var C_002_NOT_ACTIVE: String
    var C_003_ALREADY_USED: String
    var err000: String
    var err001: String
    var err002: String
    var err003: String
    var COUPON_LOGIN_MESSAGE: String
    var COUPON_LOGIN_BUTTON: String
    var ERROR_CONNECTION: String
    var ERROR_HOME_CONTENT: String
    var ERROR_GENERIC: String
    var ERROR_RETRY: String
    var COUPON_VALIDATION_ERROR: String
    var EVENT_LIST_HEADER: String
    var HOME_GUEST_AUDI_RANGE: String
    var HOME_GUEST_AUDI_RANGE_BUTTON: String
    var HOME_GUEST_COUPON_LOGIN: String
    var HOME_GUEST_COUPON_LOGIN_BUTTON: String
    var HOME_GUEST_EVENT_LIST: String
    var HOME_GUEST_EVENT_LIST_BUTTON: String
    var HOME_PREMIUM_PROGRAM_BUTTON: String
    var HOME_PREMIUM_ERROR: String
    var FOOD_EXPERIENCE_HEADER: String
    var SIDE_MENU_GUEST_COUPON_LOGIN: String
    var SIDE_MENU_GUEST_EVENT_LIST: String
    var SIDE_MENU_GUEST_HOME_PAGE: String
    var SIDE_MENU_GUEST_SETTINGS: String
    var SIDE_MENU_PREMIUM_ADE: String
    var SIDE_MENU_PREMIUM_EVENT_DETAIL: String
    var SIDE_MENU_PREMIUM_EVENT_INFO: String
    var SIDE_MENU_PREMIUM_FOOD_EXPERIENCE: String
    var SIDE_MENU_PREMIUM_LOCATION: String
    var SIDE_MENU_PREMIUM_SURVEY: String
    var HOME_ADE_TITLE: String
    var HOME_ADE_SUBTITLE: String
    var HOME_FOOD_EXPERIENCE_TITLE: String
    var HOME_FOOD_EXPERIENCE_SUBTITLE: String
    var FOOD_EXPERIENCE_ALLERGENES: String
    var HOME_PLACES_TITLE: String
    var HOME_PLACES_SUBTITLE:String
    var HOME_INFO_TITLE: String
    var HOME_SURVEY_TITLE: String
    var HOME_SURVEY_OPEN: String
    var HOME_SURVEY_EMPTY: String
    var HOME_SURVEY_CLOSED: String
    var HOME_SURVEY_SUBTITLE: String
    var GENERIC_CLOSE: String
    var GENERIC_IMAGE_ERROR_LOADING: String
    var GENERIC_DEVICE_OFFLINE: String
    var GENERIC_CONTENT_UNAVAILABLE: String
    var GENERIC_AVAILABLE_SOON: String
    var GENERIC_FORWARD: String
    var GENERIC_BACK: String
    var UPDATE_DIALOG_TITLE: String
    var UPDATE_DIALOG_TEXT: String
    var NOTIFICATION_APPBAR_TITLE: String
    var ADE_APPBAR_TITLE: String
    var FOOD_EXPERIENCE_APPBAR_TITLE: String
    var INFO_PAGE_APPBAR_TITLE: String
    var PLACES_APPBAR_TITLE: String
    var PROGRAM_APPBAR_TITLE: String
    var SURVEY_APPBAR_TITLE: String
    var SURVEY_HEADER: String
    var SURVEY_BOTTOM_HEADER: String
    var SURVEY_START_PAGE: String
    var SURVEY_FINISH_PAGE: String
    var SURVEY_FIELD_REQUIRED_MESSAGE_MANY: String
    var SURVEY_FIELD_REQUIRED_MESSAGE: String
    var SURVEY_FIELD_REQUIRED_TITLE: String
    var SURVEY_GO_TO_SURVEY: String
}
