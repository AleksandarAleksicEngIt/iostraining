//
//  DriveExperienceTerritoryCollectionViewCell.swift
//  audiEventi
//
//  Created by aleksandar.aleksic on 13.7.21..
//

import UIKit

class DriveExperienceTerritoryCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var driveExperienceTerritoryImageView: UIImageView!
}
