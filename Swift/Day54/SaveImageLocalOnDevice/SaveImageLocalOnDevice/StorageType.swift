//
//  StorageType.swift
//  SaveImageLocalOnDevice
//
//  Created by aleksandar.aleksic on 9.7.21..
//

import Foundation

enum StorageType{
    case userDefaults
    case fileSystem
}
