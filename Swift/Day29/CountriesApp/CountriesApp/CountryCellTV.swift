//
//  CountryCellTV.swift
//  CountriesApp
//
//  Created by aleksandar.aleksic on 6/4/21.
//

import UIKit

class CountryCellTV: UITableViewCell {

    @IBOutlet var countryNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
