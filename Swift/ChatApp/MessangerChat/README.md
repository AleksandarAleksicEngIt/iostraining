# Messanger Real Time Chat app

This is a app from youtube chanel iOS Academy and free playlist (https://www.youtube.com/watch?v=Mroju8T7Gdo&list=PL5PR3UyfTWvdlk-Qi-dPtJmjTj-2YIMMf&index=2). 
A beginners swift project to create a real time chat application in Swift using Firebase.

## Features 
- Facebook Log In
- Google Sign In
- Email/Pass registration / Log In
- Photo Messages
- Real Time Conversations
- Location Messages
- Search for Users
- Deleting Conversations
- User Profile
- Dark Mode Support

* Learning Swift in process
