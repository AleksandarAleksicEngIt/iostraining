//
//  SearchResult.swift
//  MessangerChat
//
//  Created by aleksandar.aleksic on 13.9.21..
//

import Foundation

struct SearchResult{
    let name: String
    let email: String
}
