//
//  ProfileViewModel.swift
//  MessangerChat
//
//  Created by aleksandar.aleksic on 13.9.21..
//

import Foundation

enum ProfileViewModelType{
    case info, logout
}

struct ProfileViewModel {
    let viewModelType: ProfileViewModelType
    let title: String
    let handler: (() -> Void)?
}
