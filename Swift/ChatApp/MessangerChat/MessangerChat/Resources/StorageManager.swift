//
//  StorageManager.swift
//  MessangerChat
//
//  Created by aleksandar.aleksic on 4.8.21..
//

import Foundation
import FirebaseStorage

/// Allows you to get, fetch and upload files to firebase storage
final class StorageManager{
    
    static let shared = StorageManager()
    
    private init() {}
    
    private let storage = Storage.storage().reference()
    
    /*
     /images/1aleksandaraleksic-gmail-com_profile_picture.png
     */
    
    public typealias UploadPictureComplition = (Result<String, Error>) -> Void
    
    /// Uploads picture to firebase storage and returns
    public func uploadProfilePicture(with data: Data, fileName: String, completion: @escaping UploadPictureComplition){
        storage.child("message_images/\(fileName)").putData(data, metadata: nil) {[weak self] metadata, error in
            
            guard let strongSelf = self else { return }
            
            guard error == nil else{
                //faild
                print("Faild to upload data to firebase for picture")
                completion(.failure(StorageErrors.faildToUpload))
                return
            }
            
            strongSelf.storage.child("message_images/\(fileName)").downloadURL { url, error in
                guard let url = url else{
                    print("Faild to get download url")
                    completion(.failure(StorageErrors.faildToGetDownloadUrl))
                    return
                }
                
                let urlString = url.absoluteString
                print("download url returned: \(urlString)")
                completion(.success(urlString))
            }
        }
    }
    
    /// Uploads image that will be sent in a conversation message
    public func uploadMessagePhoto(with data: Data, fileName: String, completion: @escaping UploadPictureComplition){
        storage.child("message_images/\(fileName)").putData(data, metadata: nil) { [weak self] metadata, error in
            
            guard let strongSelf = self else { return }
            
            guard error == nil else{
                //faild
                print("Faild to upload data to firebase for picture")
                completion(.failure(StorageErrors.faildToUpload))
                return
            }
            
            strongSelf.storage.child("message_images/\(fileName)").downloadURL { url, error in
                guard let url = url else{
                    print("Faild to get download url")
                    completion(.failure(StorageErrors.faildToGetDownloadUrl))
                    return
                }
                
                let urlString = url.absoluteString
                print("download url returned: \(urlString)")
                completion(.success(urlString))
            }
        }
    }
    
    /// Uploads video that will be sent in a conversation message
    public func uploadMessageVideo(with fileUrl: URL, fileName: String, completion: @escaping UploadPictureComplition){
        storage.child("message_videos/\(fileName)").putFile(from: fileUrl, metadata: nil) { [weak self] metadata, error in
            
            guard let strongSelf = self else { return }
            
            guard error == nil else{
                //faild
                print("Faild to upload video file to firebase for video")
                completion(.failure(StorageErrors.faildToUpload))
                return
            }
            
            strongSelf.storage.child("message_videos/\(fileName)").downloadURL { url, error in
                guard let url = url else{
                    print("Faild to get download url")
                    completion(.failure(StorageErrors.faildToGetDownloadUrl))
                    return
                }
                
                let urlString = url.absoluteString
                print("download url returned: \(urlString)")
                completion(.success(urlString))
            }
        }
    }
    
    public enum StorageErrors: Error {
        case faildToUpload
        case faildToGetDownloadUrl
    }
    
    public func downloadURL(for path: String, completion: @escaping (Result<URL, Error>) -> Void){
        let reference = storage.child(path)
        
        reference.downloadURL(completion: { url, error in
            guard let url = url, error == nil else{
                completion(.failure(StorageErrors.faildToGetDownloadUrl))
                return
            }
            completion(.success(url))
        })
    }
}
