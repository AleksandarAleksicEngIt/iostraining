//
//  Extensions.swift
//  MessangerChat
//
//  Created by aleksandar.aleksic on 22.7.21..
//

import Foundation
import UIKit

extension UIView {
    
    public var width: CGFloat{
        return frame.size.width
    }
    
    public var height: CGFloat{
        return frame.size.height
    }
    
    public var top: CGFloat{
        return frame.origin.y
    }
    
    public var bottom: CGFloat{
        return height + top
    }
    
    public var left: CGFloat{
        return frame.origin.x
    }
    
    public var right: CGFloat{
        return width + left
    }
}

extension Notification.Name{
    /// Notification when user logs in
    static let didLoginNotification = Notification.Name("didLoginNotification")
}
