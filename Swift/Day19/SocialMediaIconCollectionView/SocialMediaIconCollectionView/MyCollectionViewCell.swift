//
//  MyCollectionViewCell.swift
//  SocialMediaIconCollectionView
//
//  Created by aleksandar.aleksic on 5/21/21.
//

import UIKit

class MyCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet var label: UILabel!
    @IBOutlet var imageView: UIImageView!
    
}
